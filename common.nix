{ lib, pkgs, ... }: {
  imports = [
    ./disk-config.nix
  ];
  boot.loader.grub = {
    # no need to set devices, disko will add all devices that have a EF02 partition to the list already
    # devices = [ ];
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  nix = {
    settings = {
      allowed-users = [ "@wheel" ];
      # trusted-users = [ "root" "@wheel" ];
      experimental-features = [ "nix-command" "flakes" "repl-flake" ];
      extra-substituters = [
        "https://nix-community.cachix.org"
      ];
      extra-trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };

    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  boot = {
    tmp.cleanOnBoot = true;
  };

  security = {
    sudo.wheelNeedsPassword = lib.mkDefault false;
  };

  virtualisation.digitalOcean.setSshKeys = lib.mkForce true;

  programs.starship.enable = true;

  services = {
    services.nginx = {
      enable = true;
      enableReload = true;

      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      virtualHosts = {
        "auth.theplayground.gg" = {
          # forceSSL = true;
          # enableACME = true;

          locations."/" = {
            proxyPass = "http://127.0.0.1:3000";
          };
        };
      };
    };
    openssh.enable = true;
    postgresql = {
      enable = true;
      ensureDatabases = [ "wiki-js" ];
      ensureUsers = [{
        name = "wiki-js";
        ensureDBOwnership = true;
      }];
    };
    wiki-js = {
      enable = true;
      settings.db = {
        db = "wiki-js";
        host = "/run/postgresql";
        type = "postgres";
        user = "wiki-js";
      };
    };
  };

  systemd.services.wiki-js = {
    requires = [ "postgresql.service" ];
    after = [ "postgresql.service" ];
  };

  environment.systemPackages = with pkgs; [
    curl
    gitMinimal
    molly-guard
  ];

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 my_key bbigras@desktop"
  ];

  users.users.bbigras = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [
    ];
    # To generate a hashed password run mkpasswd.
    hashedPassword = "$y$j9T$b9tUI6HwwvF0IiRAu3c2v0$gXgnfuKbQay9PP9kejPmH5PRKvl2Oron1aXPKfHvCk6";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 my_key bbigras@desktop"
    ];
  };

  time.timeZone = "America/Montreal";

  system.stateVersion = "23.11";
}
