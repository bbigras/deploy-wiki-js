# Deploy

```sh
# set DIGITALOCEAN_TOKEN in secrets.yaml
sops secrets.yaml

# set ssh_host_rsa_key, ssh_host_rsa_key.pub, ssh_host_ed25519_key and ssh_host_ed25519_key.pub in deploy-secrets.yaml
sops deploy-secrets.yaml

tofu import digitalocean_ssh_key.default <id>

tofu apply
```
