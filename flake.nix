{
  description = "deploy-wiki-js";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
      "https://pre-commit-hooks.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "pre-commit-hooks.cachix.org-1:Pkk3Panw5AW24TOv6kz3PvLhlH8puAsJTBbOPmBo7Rc="
    ];
  };

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    flake-parts = {
      url = "flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";
    srvos = {
      url = "github:nix-community/srvos";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ flake-parts, nixpkgs, pre-commit-hooks, disko, srvos, sops-nix, ... }: flake-parts.lib.mkFlake { inherit inputs; }
    {
      systems = [
        "x86_64-linux"
      ];

      perSystem = { pkgs, system, ... }:
        let
          pre-commit-check = pre-commit-hooks.lib.${system}.run {
            src = ./.;
            hooks = {
              deadnix.enable = true;
              nixpkgs-fmt.enable = true;
              terraform-format.enable = true;
            };
            # generated files
            excludes = [
            ];
          };

          devShell = import ./shell.nix { inherit pkgs pre-commit-check; };
        in
        {
          devShells.default = devShell;
        };
    } // {
    nixosConfigurations = {
      machine1 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          disko.nixosModules.disko
          { disko.devices.disk.disk1.device = "/dev/vda"; }
          ./common.nix
          srvos.nixosModules.server
          srvos.nixosModules.hardware-digitalocean-droplet
          sops-nix.nixosModules.sops
        ];
        specialArgs = { };
      };
    };
  };
}
