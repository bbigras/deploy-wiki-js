{ pkgs, pre-commit-check }:

with pkgs;

mkShell {
  buildInputs = [
    opentofu
    sops
  ];

  shellHook = ''
    ${pre-commit-check.shellHook}
  '';
}
